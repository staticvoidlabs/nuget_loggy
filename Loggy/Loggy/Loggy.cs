﻿using Loggy.LogRequestListener;
using LoggyLib.Interfaces;
using LoggyLib.LogRequestListener;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoggyLib
{
    public class Loggy
    {
        // members
        //private static string mLogFileFullPath;
        private static RequestListenerType mLogTarget;
        private static List<ILogRequestListener> mLogRequestListeners;

        // More listener types will be implemented in the future.
        public enum RequestListenerType
        {
            FlatFile, Exception //, Database, MessageBox
        }


        /// <summary>
        /// Create log request listener instance of given type and add it to listebner list.
        /// </summary>
        /// <param name="listenerType"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static long CreateLogRequestListener(RequestListenerType listenerType, string param)
        {

            long tmpListenerId = -1;

            try
            {

                // Init listeners list if not existing.
                if (mLogRequestListeners == null)
                {
                    mLogRequestListeners = new List<ILogRequestListener>();
                }

                ILogRequestListener tmpNewLogRequestListener;

                // Create log request listener instance of given type.
                switch (listenerType)
                {

                    case RequestListenerType.FlatFile:

                        tmpNewLogRequestListener = new FlatFileLogRequestListener(param);
                        tmpListenerId = tmpNewLogRequestListener.ListenerId;

                        // Add creted log request listener to local list.
                        mLogRequestListeners.Add(tmpNewLogRequestListener);

                        break;

                    case RequestListenerType.Exception:

                        tmpNewLogRequestListener = new ExceptionLogRequestListener(param);
                        tmpListenerId = tmpNewLogRequestListener.ListenerId;

                        // Add creted log request listener to local list.
                        mLogRequestListeners.Add(tmpNewLogRequestListener);

                        break;
                }

                Log("Logging sub system initialized successful. Type: " + listenerType.ToString(), "Main.CreateLogRequestListener()", false, true);

            }
            catch (Exception ex)
            {
                Log(ex.Message, "LogHelper.StopLogging()");
            }

            return tmpListenerId;
        }

        #region Log overloads
        public static void Log(string message)
        {
            AppendLog(message, string.Empty, true, false, 0);
        }

        public static void Log(string message, string method)
        {
            AppendLog(message, method, true, false, 0);
        }

        public static void Log(string message, string method, bool isError)
        {
            AppendLog(message, method, isError, false, 0);
        }

        public static void Log(string message, string method, bool isError, bool insertBlankLineBeforeLine)
        {
            AppendLog(message, method, isError, insertBlankLineBeforeLine, 0);
        }

        public static void Log(string message, string method, bool isError, bool insertBlankLineBeforeLine, long listenerId)
        {
            AppendLog(message, method, isError, insertBlankLineBeforeLine, listenerId);
        }
        #endregion

        #region AppendLog
        private static void AppendLog(string message, string method, bool isError, bool insertBlankLineBeforeLine, long listenerId)
        {

            try
            {

                foreach (ILogRequestListener tmpListener in mLogRequestListeners)
                {

                    if (listenerId == 0 || (listenerId > 0 && listenerId == tmpListener.ListenerId))
                    {
                        tmpListener.Log(message, method, isError, insertBlankLineBeforeLine, listenerId);
                    }

                }

            }
            catch (Exception)
            {

            }

        }

        #endregion
    }


}
