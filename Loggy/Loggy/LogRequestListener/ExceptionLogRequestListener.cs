﻿using Loggy.Exceptions;
using LoggyLib.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using static LoggyLib.Loggy;

namespace Loggy.LogRequestListener
{

    public class ExceptionLogRequestListener : ILogRequestListener
    {

        // Private members.
        private RequestListenerType mLogListenerType;

        // Public fields.
        public long ListenerId { get; set; }

        // Constructors.
        public ExceptionLogRequestListener(string param)
        {

            // Init members.
            mLogListenerType = RequestListenerType.Exception;
            ListenerId = GenerateListenerId();

        }

        /// <summary>
        /// Throws exception with given message as exception message.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="method"></param>
        /// <param name="isError"></param>
        /// <param name="insertBlankLineBeforeLine"></param>
        /// <param name="listenerId"></param>
        public void Log(string message, string method, bool isError, bool insertBlankLineBeforeLine, long listenerId)
        {

            try
            {

                if (string.IsNullOrWhiteSpace(message))
                {
                    throw new Exception("Empty log message.");
                }

                if (string.IsNullOrWhiteSpace(method))
                {
                    throw new Exception("Empty method.");
                }

                // Throw exception.
                throw new LogEntryException(message, method, isError, listenerId);


            }
            catch (Exception ex)
            {

            }

        }

        private long GenerateListenerId()
        {

            return 234561;
        }

    }

}
