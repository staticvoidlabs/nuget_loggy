﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Loggy.Exceptions
{

    public class LogEntryException : Exception
    {

        // Public fields.
        public long ListenerId { get; set; }
        public string SourceMethod { get; set; }
        public bool IsError { get; set; }

        // Constructor.
        public LogEntryException(string message, string method, bool isError, long listenerId)
        {

        }

    }

}
